
package it.finanze.sanita.dem.xsd.tipodati;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dettaglioPrescrizioneVisualErogatoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dettaglioPrescrizioneVisualErogatoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="statoPresc" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="codProdPrest" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="descrProdPrest" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codGruppoEquival" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="descrGruppoEquival" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="testoLibero" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="descrTestoLiberoNote" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="nonSost" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="motivazNote" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codMotivazione" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="notaProd" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="quantita" type="{http://tipodati.xsd.dem.sanita.finanze.it}integerType"/>
 *         &lt;element name="prescrizione1" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="prescrizione2" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codProdPrestErog" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="descrProdPrestErog" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="flagErog" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="motivazSostProd" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="targa" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="dichTargaDoppia" type="{http://tipodati.xsd.dem.sanita.finanze.it}dichTargaDoppiaType" minOccurs="0"/>
 *         &lt;element name="codBranca" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoErogazioneFarm" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="prezzo" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="ticketConfezione" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="diffGenerico" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="quantitaErogata" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="dataIniErog" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType" minOccurs="0"/>
 *         &lt;element name="dataFineErog" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType" minOccurs="0"/>
 *         &lt;element name="prezzoRimborso" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="onereProd" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="scontoSSN" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="extraScontoIndustria" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="extraScontoPayback" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="extraScontoDL31052010" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codPresidio" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codReparto" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="dispFust1" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="dispFust2" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="dispFust3" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="codCatalogoPrescr" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoAccesso" type="{http://tipodati.xsd.dem.sanita.finanze.it}string1Type" minOccurs="0"/>
 *         &lt;element name="codNomenclNaz" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codCatalogoErog" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="garanziaTempiMax" type="{http://tipodati.xsd.dem.sanita.finanze.it}string1Type" minOccurs="0"/>
 *         &lt;element name="dataPrenotazione" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType" minOccurs="0"/>
 *         &lt;element name="numeroNota" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="condErogabilita" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="approprPrescrittiva" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="patologia" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoAmbulatorio" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dettaglioPrescrizioneVisualErogatoType", propOrder = {
    "statoPresc",
    "codProdPrest",
    "descrProdPrest",
    "codGruppoEquival",
    "descrGruppoEquival",
    "testoLibero",
    "descrTestoLiberoNote",
    "nonSost",
    "motivazNote",
    "codMotivazione",
    "notaProd",
    "quantita",
    "prescrizione1",
    "prescrizione2",
    "codProdPrestErog",
    "descrProdPrestErog",
    "flagErog",
    "motivazSostProd",
    "targa",
    "dichTargaDoppia",
    "codBranca",
    "tipoErogazioneFarm",
    "prezzo",
    "ticketConfezione",
    "diffGenerico",
    "quantitaErogata",
    "dataIniErog",
    "dataFineErog",
    "prezzoRimborso",
    "onereProd",
    "scontoSSN",
    "extraScontoIndustria",
    "extraScontoPayback",
    "extraScontoDL31052010",
    "codPresidio",
    "codReparto",
    "dispFust1",
    "dispFust2",
    "dispFust3",
    "codCatalogoPrescr",
    "tipoAccesso",
    "codNomenclNaz",
    "codCatalogoErog",
    "garanziaTempiMax",
    "dataPrenotazione",
    "numeroNota",
    "condErogabilita",
    "approprPrescrittiva",
    "patologia",
    "tipoAmbulatorio"
})
public class DettaglioPrescrizioneVisualErogatoType {

    @XmlElement(required = true)
    protected String statoPresc;

    protected String codProdPrest;

    protected String descrProdPrest;

    protected String codGruppoEquival;

    protected String descrGruppoEquival;

    protected String testoLibero;

    protected String descrTestoLiberoNote;

    protected String nonSost;

    protected String motivazNote;

    protected String codMotivazione;

    protected String notaProd;
    @XmlElement(required = true)
    protected String quantita;

    protected String prescrizione1;

    protected String prescrizione2;

    protected String codProdPrestErog;

    protected String descrProdPrestErog;

    protected String flagErog;

    protected String motivazSostProd;

    protected String targa;

    protected String dichTargaDoppia;

    protected String codBranca;

    protected String tipoErogazioneFarm;

    protected String prezzo;

    protected String ticketConfezione;

    protected String diffGenerico;

    protected String quantitaErogata;

    protected String dataIniErog;

    protected String dataFineErog;

    protected String prezzoRimborso;

    protected String onereProd;

    protected String scontoSSN;

    protected String extraScontoIndustria;

    protected String extraScontoPayback;

    protected String extraScontoDL31052010;

    protected String codPresidio;

    protected String codReparto;

    protected String dispFust1;

    protected String dispFust2;

    protected String dispFust3;

    protected String codCatalogoPrescr;

    protected String tipoAccesso;

    protected String codNomenclNaz;

    protected String codCatalogoErog;

    protected String garanziaTempiMax;

    protected String dataPrenotazione;

    protected String numeroNota;

    protected String condErogabilita;

    protected String approprPrescrittiva;

    protected String patologia;

    protected String tipoAmbulatorio;

    /**
     * Gets the value of the statoPresc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoPresc() {
        return statoPresc;
    }

    /**
     * Sets the value of the statoPresc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoPresc(String value) {
        this.statoPresc = value;
    }

    /**
     * Gets the value of the codProdPrest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProdPrest() {
        return codProdPrest;
    }

    /**
     * Sets the value of the codProdPrest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProdPrest(String value) {
        this.codProdPrest = value;
    }

    /**
     * Gets the value of the descrProdPrest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrProdPrest() {
        return descrProdPrest;
    }

    /**
     * Sets the value of the descrProdPrest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrProdPrest(String value) {
        this.descrProdPrest = value;
    }

    /**
     * Gets the value of the codGruppoEquival property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodGruppoEquival() {
        return codGruppoEquival;
    }

    /**
     * Sets the value of the codGruppoEquival property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodGruppoEquival(String value) {
        this.codGruppoEquival = value;
    }

    /**
     * Gets the value of the descrGruppoEquival property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrGruppoEquival() {
        return descrGruppoEquival;
    }

    /**
     * Sets the value of the descrGruppoEquival property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrGruppoEquival(String value) {
        this.descrGruppoEquival = value;
    }

    /**
     * Gets the value of the testoLibero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestoLibero() {
        return testoLibero;
    }

    /**
     * Sets the value of the testoLibero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestoLibero(String value) {
        this.testoLibero = value;
    }

    /**
     * Gets the value of the descrTestoLiberoNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrTestoLiberoNote() {
        return descrTestoLiberoNote;
    }

    /**
     * Sets the value of the descrTestoLiberoNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrTestoLiberoNote(String value) {
        this.descrTestoLiberoNote = value;
    }

    /**
     * Gets the value of the nonSost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonSost() {
        return nonSost;
    }

    /**
     * Sets the value of the nonSost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonSost(String value) {
        this.nonSost = value;
    }

    /**
     * Gets the value of the motivazNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivazNote() {
        return motivazNote;
    }

    /**
     * Sets the value of the motivazNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivazNote(String value) {
        this.motivazNote = value;
    }

    /**
     * Gets the value of the codMotivazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodMotivazione() {
        return codMotivazione;
    }

    /**
     * Sets the value of the codMotivazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodMotivazione(String value) {
        this.codMotivazione = value;
    }

    /**
     * Gets the value of the notaProd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotaProd() {
        return notaProd;
    }

    /**
     * Sets the value of the notaProd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotaProd(String value) {
        this.notaProd = value;
    }

    /**
     * Gets the value of the quantita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantita() {
        return quantita;
    }

    /**
     * Sets the value of the quantita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantita(String value) {
        this.quantita = value;
    }

    /**
     * Gets the value of the prescrizione1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrizione1() {
        return prescrizione1;
    }

    /**
     * Sets the value of the prescrizione1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrizione1(String value) {
        this.prescrizione1 = value;
    }

    /**
     * Gets the value of the prescrizione2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrizione2() {
        return prescrizione2;
    }

    /**
     * Sets the value of the prescrizione2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrizione2(String value) {
        this.prescrizione2 = value;
    }

    /**
     * Gets the value of the codProdPrestErog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProdPrestErog() {
        return codProdPrestErog;
    }

    /**
     * Sets the value of the codProdPrestErog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProdPrestErog(String value) {
        this.codProdPrestErog = value;
    }

    /**
     * Gets the value of the descrProdPrestErog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrProdPrestErog() {
        return descrProdPrestErog;
    }

    /**
     * Sets the value of the descrProdPrestErog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrProdPrestErog(String value) {
        this.descrProdPrestErog = value;
    }

    /**
     * Gets the value of the flagErog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagErog() {
        return flagErog;
    }

    /**
     * Sets the value of the flagErog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagErog(String value) {
        this.flagErog = value;
    }

    /**
     * Gets the value of the motivazSostProd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivazSostProd() {
        return motivazSostProd;
    }

    /**
     * Sets the value of the motivazSostProd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivazSostProd(String value) {
        this.motivazSostProd = value;
    }

    /**
     * Gets the value of the targa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarga() {
        return targa;
    }

    /**
     * Sets the value of the targa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarga(String value) {
        this.targa = value;
    }

    /**
     * Gets the value of the dichTargaDoppia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDichTargaDoppia() {
        return dichTargaDoppia;
    }

    /**
     * Sets the value of the dichTargaDoppia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDichTargaDoppia(String value) {
        this.dichTargaDoppia = value;
    }

    /**
     * Gets the value of the codBranca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodBranca() {
        return codBranca;
    }

    /**
     * Sets the value of the codBranca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodBranca(String value) {
        this.codBranca = value;
    }

    /**
     * Gets the value of the tipoErogazioneFarm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoErogazioneFarm() {
        return tipoErogazioneFarm;
    }

    /**
     * Sets the value of the tipoErogazioneFarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoErogazioneFarm(String value) {
        this.tipoErogazioneFarm = value;
    }

    /**
     * Gets the value of the prezzo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrezzo() {
        return prezzo;
    }

    /**
     * Sets the value of the prezzo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrezzo(String value) {
        this.prezzo = value;
    }

    /**
     * Gets the value of the ticketConfezione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketConfezione() {
        return ticketConfezione;
    }

    /**
     * Sets the value of the ticketConfezione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketConfezione(String value) {
        this.ticketConfezione = value;
    }

    /**
     * Gets the value of the diffGenerico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiffGenerico() {
        return diffGenerico;
    }

    /**
     * Sets the value of the diffGenerico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiffGenerico(String value) {
        this.diffGenerico = value;
    }

    /**
     * Gets the value of the quantitaErogata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantitaErogata() {
        return quantitaErogata;
    }

    /**
     * Sets the value of the quantitaErogata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantitaErogata(String value) {
        this.quantitaErogata = value;
    }

    /**
     * Gets the value of the dataIniErog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataIniErog() {
        return dataIniErog;
    }

    /**
     * Sets the value of the dataIniErog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataIniErog(String value) {
        this.dataIniErog = value;
    }

    /**
     * Gets the value of the dataFineErog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataFineErog() {
        return dataFineErog;
    }

    /**
     * Sets the value of the dataFineErog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataFineErog(String value) {
        this.dataFineErog = value;
    }

    /**
     * Gets the value of the prezzoRimborso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrezzoRimborso() {
        return prezzoRimborso;
    }

    /**
     * Sets the value of the prezzoRimborso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrezzoRimborso(String value) {
        this.prezzoRimborso = value;
    }

    /**
     * Gets the value of the onereProd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnereProd() {
        return onereProd;
    }

    /**
     * Sets the value of the onereProd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnereProd(String value) {
        this.onereProd = value;
    }

    /**
     * Gets the value of the scontoSSN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScontoSSN() {
        return scontoSSN;
    }

    /**
     * Sets the value of the scontoSSN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScontoSSN(String value) {
        this.scontoSSN = value;
    }

    /**
     * Gets the value of the extraScontoIndustria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraScontoIndustria() {
        return extraScontoIndustria;
    }

    /**
     * Sets the value of the extraScontoIndustria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraScontoIndustria(String value) {
        this.extraScontoIndustria = value;
    }

    /**
     * Gets the value of the extraScontoPayback property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraScontoPayback() {
        return extraScontoPayback;
    }

    /**
     * Sets the value of the extraScontoPayback property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraScontoPayback(String value) {
        this.extraScontoPayback = value;
    }

    /**
     * Gets the value of the extraScontoDL31052010 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraScontoDL31052010() {
        return extraScontoDL31052010;
    }

    /**
     * Sets the value of the extraScontoDL31052010 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraScontoDL31052010(String value) {
        this.extraScontoDL31052010 = value;
    }

    /**
     * Gets the value of the codPresidio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodPresidio() {
        return codPresidio;
    }

    /**
     * Sets the value of the codPresidio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodPresidio(String value) {
        this.codPresidio = value;
    }

    /**
     * Gets the value of the codReparto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodReparto() {
        return codReparto;
    }

    /**
     * Sets the value of the codReparto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodReparto(String value) {
        this.codReparto = value;
    }

    /**
     * Gets the value of the dispFust1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispFust1() {
        return dispFust1;
    }

    /**
     * Sets the value of the dispFust1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispFust1(String value) {
        this.dispFust1 = value;
    }

    /**
     * Gets the value of the dispFust2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispFust2() {
        return dispFust2;
    }

    /**
     * Sets the value of the dispFust2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispFust2(String value) {
        this.dispFust2 = value;
    }

    /**
     * Gets the value of the dispFust3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispFust3() {
        return dispFust3;
    }

    /**
     * Sets the value of the dispFust3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispFust3(String value) {
        this.dispFust3 = value;
    }

    /**
     * Gets the value of the codCatalogoPrescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCatalogoPrescr() {
        return codCatalogoPrescr;
    }

    /**
     * Sets the value of the codCatalogoPrescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCatalogoPrescr(String value) {
        this.codCatalogoPrescr = value;
    }

    /**
     * Gets the value of the tipoAccesso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAccesso() {
        return tipoAccesso;
    }

    /**
     * Sets the value of the tipoAccesso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAccesso(String value) {
        this.tipoAccesso = value;
    }

    /**
     * Gets the value of the codNomenclNaz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNomenclNaz() {
        return codNomenclNaz;
    }

    /**
     * Sets the value of the codNomenclNaz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNomenclNaz(String value) {
        this.codNomenclNaz = value;
    }

    /**
     * Gets the value of the codCatalogoErog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCatalogoErog() {
        return codCatalogoErog;
    }

    /**
     * Sets the value of the codCatalogoErog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCatalogoErog(String value) {
        this.codCatalogoErog = value;
    }

    /**
     * Gets the value of the garanziaTempiMax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGaranziaTempiMax() {
        return garanziaTempiMax;
    }

    /**
     * Sets the value of the garanziaTempiMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGaranziaTempiMax(String value) {
        this.garanziaTempiMax = value;
    }

    /**
     * Gets the value of the dataPrenotazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataPrenotazione() {
        return dataPrenotazione;
    }

    /**
     * Sets the value of the dataPrenotazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataPrenotazione(String value) {
        this.dataPrenotazione = value;
    }

    /**
     * Gets the value of the numeroNota property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroNota() {
        return numeroNota;
    }

    /**
     * Sets the value of the numeroNota property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroNota(String value) {
        this.numeroNota = value;
    }

    /**
     * Gets the value of the condErogabilita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCondErogabilita() {
        return condErogabilita;
    }

    /**
     * Sets the value of the condErogabilita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCondErogabilita(String value) {
        this.condErogabilita = value;
    }

    /**
     * Gets the value of the approprPrescrittiva property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproprPrescrittiva() {
        return approprPrescrittiva;
    }

    /**
     * Sets the value of the approprPrescrittiva property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproprPrescrittiva(String value) {
        this.approprPrescrittiva = value;
    }

    /**
     * Gets the value of the patologia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatologia() {
        return patologia;
    }

    /**
     * Sets the value of the patologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatologia(String value) {
        this.patologia = value;
    }

    /**
     * Gets the value of the tipoAmbulatorio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAmbulatorio() {
        return tipoAmbulatorio;
    }

    /**
     * Sets the value of the tipoAmbulatorio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAmbulatorio(String value) {
        this.tipoAmbulatorio = value;
    }

}
