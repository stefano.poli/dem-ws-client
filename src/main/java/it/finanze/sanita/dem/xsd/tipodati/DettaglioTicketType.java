
package it.finanze.sanita.dem.xsd.tipodati;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dettaglioTicketType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dettaglioTicketType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codProdPrestErog" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="progrPresc" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="ticketConfezione" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="diffGenerico" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="prezzo" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dettaglioTicketType", propOrder = {
    "codProdPrestErog",
    "progrPresc",
    "ticketConfezione",
    "diffGenerico",
    "prezzo"
})
public class DettaglioTicketType {

    @XmlElement(required = true)
    protected String codProdPrestErog;
    @XmlElement(required = true)
    protected String progrPresc;
    @XmlElement(required = true)
    protected String ticketConfezione;
    @XmlElement(required = true)
    protected String diffGenerico;
    @XmlElement(required = true)
    protected String prezzo;

    /**
     * Gets the value of the codProdPrestErog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProdPrestErog() {
        return codProdPrestErog;
    }

    /**
     * Sets the value of the codProdPrestErog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProdPrestErog(String value) {
        this.codProdPrestErog = value;
    }

    /**
     * Gets the value of the progrPresc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgrPresc() {
        return progrPresc;
    }

    /**
     * Sets the value of the progrPresc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgrPresc(String value) {
        this.progrPresc = value;
    }

    /**
     * Gets the value of the ticketConfezione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketConfezione() {
        return ticketConfezione;
    }

    /**
     * Sets the value of the ticketConfezione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketConfezione(String value) {
        this.ticketConfezione = value;
    }

    /**
     * Gets the value of the diffGenerico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiffGenerico() {
        return diffGenerico;
    }

    /**
     * Sets the value of the diffGenerico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiffGenerico(String value) {
        this.diffGenerico = value;
    }

    /**
     * Gets the value of the prezzo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrezzo() {
        return prezzo;
    }

    /**
     * Sets the value of the prezzo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrezzo(String value) {
        this.prezzo = value;
    }

}
