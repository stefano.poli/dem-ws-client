
package it.finanze.sanita.dem.xsd.visualizzaerogatoricevuta;

import it.finanze.sanita.dem.xsd.tipodati.ElencoComunicazioniType;
import it.finanze.sanita.dem.xsd.tipodati.ElencoDettagliPrescrVisualErogatoType;
import it.finanze.sanita.dem.xsd.tipodati.ElencoErroriRicetteType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nre" type="{http://tipodati.xsd.dem.sanita.finanze.it}nreType" minOccurs="0"/>
 *         &lt;element name="cfMedico1" type="{http://tipodati.xsd.dem.sanita.finanze.it}cfMedicoType" minOccurs="0"/>
 *         &lt;element name="cfMedico2" type="{http://tipodati.xsd.dem.sanita.finanze.it}cfMedicoType" minOccurs="0"/>
 *         &lt;element name="codRegione" type="{http://tipodati.xsd.dem.sanita.finanze.it}codRegioneType" minOccurs="0"/>
 *         &lt;element name="codASLAo" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codStruttura" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codSpecializzazione" type="{http://tipodati.xsd.dem.sanita.finanze.it}codSpecializzazioneType" minOccurs="0"/>
 *         &lt;element name="testata1" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="testata2" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoRic" type="{http://tipodati.xsd.dem.sanita.finanze.it}tipoRicettaType" minOccurs="0"/>
 *         &lt;element name="codiceAss" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="cognNome" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="indirizzo" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="oscuramDati" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="numTessSasn" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="socNavigaz" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoPrescrizione" type="{http://tipodati.xsd.dem.sanita.finanze.it}tipoPrescType" minOccurs="0"/>
 *         &lt;element name="ricettaInterna" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codEsenzione" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="nonEsente" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="reddito" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codDiagnosi" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="descrizioneDiagnosi" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="dataCompilazione" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType" minOccurs="0"/>
 *         &lt;element name="tipoVisita" type="{http://tipodati.xsd.dem.sanita.finanze.it}tipoVisitaType" minOccurs="0"/>
 *         &lt;element name="dispReg" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="provAssistito" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="aslAssistito" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="indicazionePrescr" type="{http://tipodati.xsd.dem.sanita.finanze.it}indicPrescType" minOccurs="0"/>
 *         &lt;element name="altro" type="{http://tipodati.xsd.dem.sanita.finanze.it}indicPrescType" minOccurs="0"/>
 *         &lt;element name="classePriorita" type="{http://tipodati.xsd.dem.sanita.finanze.it}prioritaType" minOccurs="0"/>
 *         &lt;element name="statoEstero" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="istituzCompetente" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="numIdentPers" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="numIdentTess" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="dataNascitaEstero" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType" minOccurs="0"/>
 *         &lt;element name="dataScadTessera" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType" minOccurs="0"/>
 *         &lt;element name="statoProcesso" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="chiusuraDiff" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="chiusuraForzata" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="prescrizioneFruita" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoErogazioneSpec" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="ticket" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="quotaFissa" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="franchigia" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="galDirChiamAltro" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="dataSpedizione" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType" minOccurs="0"/>
 *         &lt;element name="dispRic1" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="dispRic2" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="dispRic3" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="ElencoDettagliPrescrVisualErogato" type="{http://tipodati.xsd.dem.sanita.finanze.it}elencoDettagliPrescrVisualErogatoType" minOccurs="0"/>
 *         &lt;element name="codAutenticazioneMedico" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codAutenticazioneErogatore" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codEsitoVisualizzazione" type="{http://tipodati.xsd.dem.sanita.finanze.it}codEsitoType"/>
 *         &lt;element name="ElencoErroriRicette" type="{http://tipodati.xsd.dem.sanita.finanze.it}elencoErroriRicetteType" minOccurs="0"/>
 *         &lt;element name="ElencoComunicazioni" type="{http://tipodati.xsd.dem.sanita.finanze.it}elencoComunicazioniType" minOccurs="0"/>
 *         &lt;element name="codEseNaz" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nre",
    "cfMedico1",
    "cfMedico2",
    "codRegione",
    "codASLAo",
    "codStruttura",
    "codSpecializzazione",
    "testata1",
    "testata2",
    "tipoRic",
    "codiceAss",
    "cognNome",
    "indirizzo",
    "oscuramDati",
    "numTessSasn",
    "socNavigaz",
    "tipoPrescrizione",
    "ricettaInterna",
    "codEsenzione",
    "nonEsente",
    "reddito",
    "codDiagnosi",
    "descrizioneDiagnosi",
    "dataCompilazione",
    "tipoVisita",
    "dispReg",
    "provAssistito",
    "aslAssistito",
    "indicazionePrescr",
    "altro",
    "classePriorita",
    "statoEstero",
    "istituzCompetente",
    "numIdentPers",
    "numIdentTess",
    "dataNascitaEstero",
    "dataScadTessera",
    "statoProcesso",
    "chiusuraDiff",
    "chiusuraForzata",
    "prescrizioneFruita",
    "tipoErogazioneSpec",
    "ticket",
    "quotaFissa",
    "franchigia",
    "galDirChiamAltro",
    "dataSpedizione",
    "dispRic1",
    "dispRic2",
    "dispRic3",
    "elencoDettagliPrescrVisualErogato",
    "codAutenticazioneMedico",
    "codAutenticazioneErogatore",
    "codEsitoVisualizzazione",
    "elencoErroriRicette",
    "elencoComunicazioni",
    "codEseNaz"
})
@XmlRootElement(name = "VisualizzaErogatoRicevuta")
public class VisualizzaErogatoRicevuta {


    protected String nre;

    protected String cfMedico1;

    protected String cfMedico2;

    protected String codRegione;

    protected String codASLAo;

    protected String codStruttura;

    protected String codSpecializzazione;

    protected String testata1;

    protected String testata2;

    protected String tipoRic;

    protected String codiceAss;

    protected String cognNome;

    protected String indirizzo;

    protected String oscuramDati;

    protected String numTessSasn;

    protected String socNavigaz;

    protected String tipoPrescrizione;

    protected String ricettaInterna;

    protected String codEsenzione;

    protected String nonEsente;

    protected String reddito;

    protected String codDiagnosi;

    protected String descrizioneDiagnosi;

    protected String dataCompilazione;

    protected String tipoVisita;

    protected String dispReg;

    protected String provAssistito;

    protected String aslAssistito;

    protected String indicazionePrescr;

    protected String altro;

    protected String classePriorita;

    protected String statoEstero;

    protected String istituzCompetente;

    protected String numIdentPers;

    protected String numIdentTess;

    protected String dataNascitaEstero;

    protected String dataScadTessera;

    protected String statoProcesso;

    protected String chiusuraDiff;

    protected String chiusuraForzata;

    protected String prescrizioneFruita;

    protected String tipoErogazioneSpec;

    protected String ticket;

    protected String quotaFissa;

    protected String franchigia;

    protected String galDirChiamAltro;

    protected String dataSpedizione;

    protected String dispRic1;

    protected String dispRic2;

    protected String dispRic3;
    @XmlElement(name = "ElencoDettagliPrescrVisualErogato")
    protected ElencoDettagliPrescrVisualErogatoType elencoDettagliPrescrVisualErogato;

    protected String codAutenticazioneMedico;

    protected String codAutenticazioneErogatore;
    @XmlElement(required = true)
    protected String codEsitoVisualizzazione;
    @XmlElement(name = "ElencoErroriRicette")
    protected ElencoErroriRicetteType elencoErroriRicette;
    @XmlElement(name = "ElencoComunicazioni")
    protected ElencoComunicazioniType elencoComunicazioni;

    protected String codEseNaz;

    /**
     * Gets the value of the nre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNre() {
        return nre;
    }

    /**
     * Sets the value of the nre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNre(String value) {
        this.nre = value;
    }

    /**
     * Gets the value of the cfMedico1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCfMedico1() {
        return cfMedico1;
    }

    /**
     * Sets the value of the cfMedico1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCfMedico1(String value) {
        this.cfMedico1 = value;
    }

    /**
     * Gets the value of the cfMedico2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCfMedico2() {
        return cfMedico2;
    }

    /**
     * Sets the value of the cfMedico2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCfMedico2(String value) {
        this.cfMedico2 = value;
    }

    /**
     * Gets the value of the codRegione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegione() {
        return codRegione;
    }

    /**
     * Sets the value of the codRegione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegione(String value) {
        this.codRegione = value;
    }

    /**
     * Gets the value of the codASLAo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodASLAo() {
        return codASLAo;
    }

    /**
     * Sets the value of the codASLAo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodASLAo(String value) {
        this.codASLAo = value;
    }

    /**
     * Gets the value of the codStruttura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodStruttura() {
        return codStruttura;
    }

    /**
     * Sets the value of the codStruttura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodStruttura(String value) {
        this.codStruttura = value;
    }

    /**
     * Gets the value of the codSpecializzazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSpecializzazione() {
        return codSpecializzazione;
    }

    /**
     * Sets the value of the codSpecializzazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSpecializzazione(String value) {
        this.codSpecializzazione = value;
    }

    /**
     * Gets the value of the testata1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestata1() {
        return testata1;
    }

    /**
     * Sets the value of the testata1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestata1(String value) {
        this.testata1 = value;
    }

    /**
     * Gets the value of the testata2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestata2() {
        return testata2;
    }

    /**
     * Sets the value of the testata2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestata2(String value) {
        this.testata2 = value;
    }

    /**
     * Gets the value of the tipoRic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoRic() {
        return tipoRic;
    }

    /**
     * Sets the value of the tipoRic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoRic(String value) {
        this.tipoRic = value;
    }

    /**
     * Gets the value of the codiceAss property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAss() {
        return codiceAss;
    }

    /**
     * Sets the value of the codiceAss property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAss(String value) {
        this.codiceAss = value;
    }

    /**
     * Gets the value of the cognNome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCognNome() {
        return cognNome;
    }

    /**
     * Sets the value of the cognNome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCognNome(String value) {
        this.cognNome = value;
    }

    /**
     * Gets the value of the indirizzo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirizzo() {
        return indirizzo;
    }

    /**
     * Sets the value of the indirizzo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirizzo(String value) {
        this.indirizzo = value;
    }

    /**
     * Gets the value of the oscuramDati property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOscuramDati() {
        return oscuramDati;
    }

    /**
     * Sets the value of the oscuramDati property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOscuramDati(String value) {
        this.oscuramDati = value;
    }

    /**
     * Gets the value of the numTessSasn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTessSasn() {
        return numTessSasn;
    }

    /**
     * Sets the value of the numTessSasn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTessSasn(String value) {
        this.numTessSasn = value;
    }

    /**
     * Gets the value of the socNavigaz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocNavigaz() {
        return socNavigaz;
    }

    /**
     * Sets the value of the socNavigaz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocNavigaz(String value) {
        this.socNavigaz = value;
    }

    /**
     * Gets the value of the tipoPrescrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPrescrizione() {
        return tipoPrescrizione;
    }

    /**
     * Sets the value of the tipoPrescrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPrescrizione(String value) {
        this.tipoPrescrizione = value;
    }

    /**
     * Gets the value of the ricettaInterna property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRicettaInterna() {
        return ricettaInterna;
    }

    /**
     * Sets the value of the ricettaInterna property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRicettaInterna(String value) {
        this.ricettaInterna = value;
    }

    /**
     * Gets the value of the codEsenzione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEsenzione() {
        return codEsenzione;
    }

    /**
     * Sets the value of the codEsenzione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEsenzione(String value) {
        this.codEsenzione = value;
    }

    /**
     * Gets the value of the nonEsente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonEsente() {
        return nonEsente;
    }

    /**
     * Sets the value of the nonEsente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonEsente(String value) {
        this.nonEsente = value;
    }

    /**
     * Gets the value of the reddito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReddito() {
        return reddito;
    }

    /**
     * Sets the value of the reddito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReddito(String value) {
        this.reddito = value;
    }

    /**
     * Gets the value of the codDiagnosi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodDiagnosi() {
        return codDiagnosi;
    }

    /**
     * Sets the value of the codDiagnosi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodDiagnosi(String value) {
        this.codDiagnosi = value;
    }

    /**
     * Gets the value of the descrizioneDiagnosi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneDiagnosi() {
        return descrizioneDiagnosi;
    }

    /**
     * Sets the value of the descrizioneDiagnosi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneDiagnosi(String value) {
        this.descrizioneDiagnosi = value;
    }

    /**
     * Gets the value of the dataCompilazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCompilazione() {
        return dataCompilazione;
    }

    /**
     * Sets the value of the dataCompilazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCompilazione(String value) {
        this.dataCompilazione = value;
    }

    /**
     * Gets the value of the tipoVisita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoVisita() {
        return tipoVisita;
    }

    /**
     * Sets the value of the tipoVisita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoVisita(String value) {
        this.tipoVisita = value;
    }

    /**
     * Gets the value of the dispReg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispReg() {
        return dispReg;
    }

    /**
     * Sets the value of the dispReg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispReg(String value) {
        this.dispReg = value;
    }

    /**
     * Gets the value of the provAssistito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvAssistito() {
        return provAssistito;
    }

    /**
     * Sets the value of the provAssistito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvAssistito(String value) {
        this.provAssistito = value;
    }

    /**
     * Gets the value of the aslAssistito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAslAssistito() {
        return aslAssistito;
    }

    /**
     * Sets the value of the aslAssistito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAslAssistito(String value) {
        this.aslAssistito = value;
    }

    /**
     * Gets the value of the indicazionePrescr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicazionePrescr() {
        return indicazionePrescr;
    }

    /**
     * Sets the value of the indicazionePrescr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicazionePrescr(String value) {
        this.indicazionePrescr = value;
    }

    /**
     * Gets the value of the altro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltro() {
        return altro;
    }

    /**
     * Sets the value of the altro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltro(String value) {
        this.altro = value;
    }

    /**
     * Gets the value of the classePriorita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassePriorita() {
        return classePriorita;
    }

    /**
     * Sets the value of the classePriorita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassePriorita(String value) {
        this.classePriorita = value;
    }

    /**
     * Gets the value of the statoEstero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoEstero() {
        return statoEstero;
    }

    /**
     * Sets the value of the statoEstero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoEstero(String value) {
        this.statoEstero = value;
    }

    /**
     * Gets the value of the istituzCompetente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIstituzCompetente() {
        return istituzCompetente;
    }

    /**
     * Sets the value of the istituzCompetente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIstituzCompetente(String value) {
        this.istituzCompetente = value;
    }

    /**
     * Gets the value of the numIdentPers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumIdentPers() {
        return numIdentPers;
    }

    /**
     * Sets the value of the numIdentPers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumIdentPers(String value) {
        this.numIdentPers = value;
    }

    /**
     * Gets the value of the numIdentTess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumIdentTess() {
        return numIdentTess;
    }

    /**
     * Sets the value of the numIdentTess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumIdentTess(String value) {
        this.numIdentTess = value;
    }

    /**
     * Gets the value of the dataNascitaEstero property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataNascitaEstero() {
        return dataNascitaEstero;
    }

    /**
     * Sets the value of the dataNascitaEstero property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataNascitaEstero(String value) {
        this.dataNascitaEstero = value;
    }

    /**
     * Gets the value of the dataScadTessera property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataScadTessera() {
        return dataScadTessera;
    }

    /**
     * Sets the value of the dataScadTessera property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataScadTessera(String value) {
        this.dataScadTessera = value;
    }

    /**
     * Gets the value of the statoProcesso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoProcesso() {
        return statoProcesso;
    }

    /**
     * Sets the value of the statoProcesso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoProcesso(String value) {
        this.statoProcesso = value;
    }

    /**
     * Gets the value of the chiusuraDiff property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiusuraDiff() {
        return chiusuraDiff;
    }

    /**
     * Sets the value of the chiusuraDiff property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiusuraDiff(String value) {
        this.chiusuraDiff = value;
    }

    /**
     * Gets the value of the chiusuraForzata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiusuraForzata() {
        return chiusuraForzata;
    }

    /**
     * Sets the value of the chiusuraForzata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiusuraForzata(String value) {
        this.chiusuraForzata = value;
    }

    /**
     * Gets the value of the prescrizioneFruita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrizioneFruita() {
        return prescrizioneFruita;
    }

    /**
     * Sets the value of the prescrizioneFruita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrizioneFruita(String value) {
        this.prescrizioneFruita = value;
    }

    /**
     * Gets the value of the tipoErogazioneSpec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoErogazioneSpec() {
        return tipoErogazioneSpec;
    }

    /**
     * Sets the value of the tipoErogazioneSpec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoErogazioneSpec(String value) {
        this.tipoErogazioneSpec = value;
    }

    /**
     * Gets the value of the ticket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * Sets the value of the ticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicket(String value) {
        this.ticket = value;
    }

    /**
     * Gets the value of the quotaFissa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuotaFissa() {
        return quotaFissa;
    }

    /**
     * Sets the value of the quotaFissa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuotaFissa(String value) {
        this.quotaFissa = value;
    }

    /**
     * Gets the value of the franchigia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFranchigia() {
        return franchigia;
    }

    /**
     * Sets the value of the franchigia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFranchigia(String value) {
        this.franchigia = value;
    }

    /**
     * Gets the value of the galDirChiamAltro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGalDirChiamAltro() {
        return galDirChiamAltro;
    }

    /**
     * Sets the value of the galDirChiamAltro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGalDirChiamAltro(String value) {
        this.galDirChiamAltro = value;
    }

    /**
     * Gets the value of the dataSpedizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataSpedizione() {
        return dataSpedizione;
    }

    /**
     * Sets the value of the dataSpedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataSpedizione(String value) {
        this.dataSpedizione = value;
    }

    /**
     * Gets the value of the dispRic1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispRic1() {
        return dispRic1;
    }

    /**
     * Sets the value of the dispRic1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispRic1(String value) {
        this.dispRic1 = value;
    }

    /**
     * Gets the value of the dispRic2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispRic2() {
        return dispRic2;
    }

    /**
     * Sets the value of the dispRic2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispRic2(String value) {
        this.dispRic2 = value;
    }

    /**
     * Gets the value of the dispRic3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispRic3() {
        return dispRic3;
    }

    /**
     * Sets the value of the dispRic3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispRic3(String value) {
        this.dispRic3 = value;
    }

    /**
     * Gets the value of the elencoDettagliPrescrVisualErogato property.
     * 
     * @return
     *     possible object is
     *     {@link ElencoDettagliPrescrVisualErogatoType }
     *     
     */
    public ElencoDettagliPrescrVisualErogatoType getElencoDettagliPrescrVisualErogato() {
        return elencoDettagliPrescrVisualErogato;
    }

    /**
     * Sets the value of the elencoDettagliPrescrVisualErogato property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElencoDettagliPrescrVisualErogatoType }
     *     
     */
    public void setElencoDettagliPrescrVisualErogato(ElencoDettagliPrescrVisualErogatoType value) {
        this.elencoDettagliPrescrVisualErogato = value;
    }

    /**
     * Gets the value of the codAutenticazioneMedico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAutenticazioneMedico() {
        return codAutenticazioneMedico;
    }

    /**
     * Sets the value of the codAutenticazioneMedico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAutenticazioneMedico(String value) {
        this.codAutenticazioneMedico = value;
    }

    /**
     * Gets the value of the codAutenticazioneErogatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAutenticazioneErogatore() {
        return codAutenticazioneErogatore;
    }

    /**
     * Sets the value of the codAutenticazioneErogatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAutenticazioneErogatore(String value) {
        this.codAutenticazioneErogatore = value;
    }

    /**
     * Gets the value of the codEsitoVisualizzazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEsitoVisualizzazione() {
        return codEsitoVisualizzazione;
    }

    /**
     * Sets the value of the codEsitoVisualizzazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEsitoVisualizzazione(String value) {
        this.codEsitoVisualizzazione = value;
    }

    /**
     * Gets the value of the elencoErroriRicette property.
     * 
     * @return
     *     possible object is
     *     {@link ElencoErroriRicetteType }
     *     
     */
    public ElencoErroriRicetteType getElencoErroriRicette() {
        return elencoErroriRicette;
    }

    /**
     * Sets the value of the elencoErroriRicette property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElencoErroriRicetteType }
     *     
     */
    public void setElencoErroriRicette(ElencoErroriRicetteType value) {
        this.elencoErroriRicette = value;
    }

    /**
     * Gets the value of the elencoComunicazioni property.
     * 
     * @return
     *     possible object is
     *     {@link ElencoComunicazioniType }
     *     
     */
    public ElencoComunicazioniType getElencoComunicazioni() {
        return elencoComunicazioni;
    }

    /**
     * Sets the value of the elencoComunicazioni property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElencoComunicazioniType }
     *     
     */
    public void setElencoComunicazioni(ElencoComunicazioniType value) {
        this.elencoComunicazioni = value;
    }

    /**
     * Gets the value of the codEseNaz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEseNaz() {
        return codEseNaz;
    }

    /**
     * Sets the value of the codEseNaz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEseNaz(String value) {
        this.codEseNaz = value;
    }

}
