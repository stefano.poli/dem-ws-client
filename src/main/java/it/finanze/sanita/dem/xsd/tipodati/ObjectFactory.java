
package it.finanze.sanita.dem.xsd.tipodati;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.finanze.sanita.dem.xsd.demricettaerogato package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitionselement declarations and model
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.finanze.sanita.dem.xsd.demricettaerogato
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ElencoErroriRicetteType }
     * 
     */
    public ElencoErroriRicetteType createElencoErroriRicetteType() {
        return new ElencoErroriRicetteType();
    }

    /**
     * Create an instance of {@link ElencoComunicazioniType }
     * 
     */
    public ElencoComunicazioniType createElencoComunicazioniType() {
        return new ElencoComunicazioniType();
    }

    /**
     * Create an instance of {@link ElencoDettagliPrescrVisualErogatoType }
     * 
     */
    public ElencoDettagliPrescrVisualErogatoType createElencoDettagliPrescrVisualErogatoType() {
        return new ElencoDettagliPrescrVisualErogatoType();
    }

    /**
     * Create an instance of {@link ComunicazioneType }
     * 
     */
    public ComunicazioneType createComunicazioneType() {
        return new ComunicazioneType();
    }

    /**
     * Create an instance of {@link DettaglioPrescrizioneVisualErogatoType }
     * 
     */
    public DettaglioPrescrizioneVisualErogatoType createDettaglioPrescrizioneVisualErogatoType() {
        return new DettaglioPrescrizioneVisualErogatoType();
    }

    /**
     * Create an instance of {@link ErroreRicettaType }
     * 
     */
    public ErroreRicettaType createErroreRicettaType() {
        return new ErroreRicettaType();
    }

    /**
     * Create an instance of {@link DettaglioPrescrizioneInvioErogatoType }
     *
     */
    public DettaglioPrescrizioneInvioErogatoType createDettaglioPrescrizioneInvioErogatoType() {
        return new DettaglioPrescrizioneInvioErogatoType();
    }

    /**
     * Create an instance of {@link DettaglioTicketType }
     *
     */
    public DettaglioTicketType createDettaglioTicketType() {
        return new DettaglioTicketType();
    }

    /**
     * Create an instance of {@link ElencoDettagliPrescrInviiErogatoType }
     *
     */
    public ElencoDettagliPrescrInviiErogatoType createElencoDettagliPrescrInviiErogatoType() {
        return new ElencoDettagliPrescrInviiErogatoType();
    }

    /**
     * Create an instance of {@link ElencoDettagliTicketType }
     *
     */
    public ElencoDettagliTicketType createElencoDettagliTicketType() {
        return new ElencoDettagliTicketType();
    }

}
