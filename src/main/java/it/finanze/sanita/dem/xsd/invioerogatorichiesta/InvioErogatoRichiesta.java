
package it.finanze.sanita.dem.xsd.invioerogatorichiesta;

import it.finanze.sanita.dem.xsd.tipodati.ElencoDettagliPrescrInviiErogatoType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pinCode" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="codiceRegioneErogatore" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="codiceAslErogatore" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="codiceSsaErogatore" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="pwd" type="{http://tipodati.xsd.dem.sanita.finanze.it}pwdType"/>
 *         &lt;element name="nre" type="{http://tipodati.xsd.dem.sanita.finanze.it}nreType"/>
 *         &lt;element name="cfAssistito" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoOperazione" type="{http://tipodati.xsd.dem.sanita.finanze.it}tipoOperazioneType"/>
 *         &lt;element name="prescrizioneFruita" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoErogazioneSpec" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="ticket" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="quotaFissa" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="franchigia" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="galDirChiamAltro" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="reddito" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="dataSpedizione" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType"/>
 *         &lt;element name="dispRic1" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="dispRic2" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="dispRic3" type="{http://tipodati.xsd.dem.sanita.finanze.it}string256Type" minOccurs="0"/>
 *         &lt;element name="ElencoDettagliPrescrInviiErogato" type="{http://tipodati.xsd.dem.sanita.finanze.it}elencoDettagliPrescrInviiErogatoType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pinCode",
    "codiceRegioneErogatore",
    "codiceAslErogatore",
    "codiceSsaErogatore",
    "pwd",
    "nre",
    "cfAssistito",
    "tipoOperazione",
    "prescrizioneFruita",
    "tipoErogazioneSpec",
    "ticket",
    "quotaFissa",
    "franchigia",
    "galDirChiamAltro",
    "reddito",
    "dataSpedizione",
    "dispRic1",
    "dispRic2",
    "dispRic3",
    "elencoDettagliPrescrInviiErogato"
})
@XmlRootElement(name = "InvioErogatoRichiesta")
public class InvioErogatoRichiesta {

    @XmlElement(required = true)
    protected String pinCode;
    @XmlElement(required = true)
    protected String codiceRegioneErogatore;
    @XmlElement(required = true)
    protected String codiceAslErogatore;
    @XmlElement(required = true)
    protected String codiceSsaErogatore;
    @XmlElement(required = true)
    protected String pwd;
    @XmlElement(required = true)
    protected String nre;

    protected String cfAssistito;
    @XmlElement(required = true)
    protected String tipoOperazione;

    protected String prescrizioneFruita;

    protected String tipoErogazioneSpec;

    protected String ticket;
    @XmlElement(required = true)
    protected String quotaFissa;
    @XmlElement(required = true)
    protected String franchigia;
    @XmlElement(required = true)
    protected String galDirChiamAltro;

    protected String reddito;
    @XmlElement(required = true)
    protected String dataSpedizione;

    protected String dispRic1;

    protected String dispRic2;

    protected String dispRic3;
    @XmlElement(name = "ElencoDettagliPrescrInviiErogato", required = true)
    protected ElencoDettagliPrescrInviiErogatoType elencoDettagliPrescrInviiErogato;

    /**
     * Gets the value of the pinCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPinCode() {
        return pinCode;
    }

    /**
     * Sets the value of the pinCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPinCode(String value) {
        this.pinCode = value;
    }

    /**
     * Gets the value of the codiceRegioneErogatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceRegioneErogatore() {
        return codiceRegioneErogatore;
    }

    /**
     * Sets the value of the codiceRegioneErogatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceRegioneErogatore(String value) {
        this.codiceRegioneErogatore = value;
    }

    /**
     * Gets the value of the codiceAslErogatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAslErogatore() {
        return codiceAslErogatore;
    }

    /**
     * Sets the value of the codiceAslErogatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAslErogatore(String value) {
        this.codiceAslErogatore = value;
    }

    /**
     * Gets the value of the codiceSsaErogatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceSsaErogatore() {
        return codiceSsaErogatore;
    }

    /**
     * Sets the value of the codiceSsaErogatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceSsaErogatore(String value) {
        this.codiceSsaErogatore = value;
    }

    /**
     * Gets the value of the pwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * Sets the value of the pwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPwd(String value) {
        this.pwd = value;
    }

    /**
     * Gets the value of the nre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNre() {
        return nre;
    }

    /**
     * Sets the value of the nre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNre(String value) {
        this.nre = value;
    }

    /**
     * Gets the value of the cfAssistito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCfAssistito() {
        return cfAssistito;
    }

    /**
     * Sets the value of the cfAssistito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCfAssistito(String value) {
        this.cfAssistito = value;
    }

    /**
     * Gets the value of the tipoOperazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Sets the value of the tipoOperazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperazione(String value) {
        this.tipoOperazione = value;
    }

    /**
     * Gets the value of the prescrizioneFruita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrescrizioneFruita() {
        return prescrizioneFruita;
    }

    /**
     * Sets the value of the prescrizioneFruita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrescrizioneFruita(String value) {
        this.prescrizioneFruita = value;
    }

    /**
     * Gets the value of the tipoErogazioneSpec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoErogazioneSpec() {
        return tipoErogazioneSpec;
    }

    /**
     * Sets the value of the tipoErogazioneSpec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoErogazioneSpec(String value) {
        this.tipoErogazioneSpec = value;
    }

    /**
     * Gets the value of the ticket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * Sets the value of the ticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicket(String value) {
        this.ticket = value;
    }

    /**
     * Gets the value of the quotaFissa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuotaFissa() {
        return quotaFissa;
    }

    /**
     * Sets the value of the quotaFissa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuotaFissa(String value) {
        this.quotaFissa = value;
    }

    /**
     * Gets the value of the franchigia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFranchigia() {
        return franchigia;
    }

    /**
     * Sets the value of the franchigia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFranchigia(String value) {
        this.franchigia = value;
    }

    /**
     * Gets the value of the galDirChiamAltro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGalDirChiamAltro() {
        return galDirChiamAltro;
    }

    /**
     * Sets the value of the galDirChiamAltro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGalDirChiamAltro(String value) {
        this.galDirChiamAltro = value;
    }

    /**
     * Gets the value of the reddito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReddito() {
        return reddito;
    }

    /**
     * Sets the value of the reddito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReddito(String value) {
        this.reddito = value;
    }

    /**
     * Gets the value of the dataSpedizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataSpedizione() {
        return dataSpedizione;
    }

    /**
     * Sets the value of the dataSpedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataSpedizione(String value) {
        this.dataSpedizione = value;
    }

    /**
     * Gets the value of the dispRic1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispRic1() {
        return dispRic1;
    }

    /**
     * Sets the value of the dispRic1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispRic1(String value) {
        this.dispRic1 = value;
    }

    /**
     * Gets the value of the dispRic2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispRic2() {
        return dispRic2;
    }

    /**
     * Sets the value of the dispRic2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispRic2(String value) {
        this.dispRic2 = value;
    }

    /**
     * Gets the value of the dispRic3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispRic3() {
        return dispRic3;
    }

    /**
     * Sets the value of the dispRic3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispRic3(String value) {
        this.dispRic3 = value;
    }

    /**
     * Gets the value of the elencoDettagliPrescrInviiErogato property.
     * 
     * @return
     *     possible object is
     *     {@link ElencoDettagliPrescrInviiErogatoType }
     *     
     */
    public ElencoDettagliPrescrInviiErogatoType getElencoDettagliPrescrInviiErogato() {
        return elencoDettagliPrescrInviiErogato;
    }

    /**
     * Sets the value of the elencoDettagliPrescrInviiErogato property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElencoDettagliPrescrInviiErogatoType }
     *     
     */
    public void setElencoDettagliPrescrInviiErogato(ElencoDettagliPrescrInviiErogatoType value) {
        this.elencoDettagliPrescrInviiErogato = value;
    }

}
