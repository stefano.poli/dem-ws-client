
package it.finanze.sanita.dem.xsd.sospendierogatorichiesta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pinCode" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="codiceRegioneErogatore" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="codiceAslErogatore" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="codiceSsaErogatore" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType"/>
 *         &lt;element name="pwd" type="{http://tipodati.xsd.dem.sanita.finanze.it}pwdType"/>
 *         &lt;element name="nre" type="{http://tipodati.xsd.dem.sanita.finanze.it}nreType"/>
 *         &lt;element name="cfAssistito" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="tipoOperazione" type="{http://tipodati.xsd.dem.sanita.finanze.it}tipoOperazioneType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pinCode",
    "codiceRegioneErogatore",
    "codiceAslErogatore",
    "codiceSsaErogatore",
    "pwd",
    "nre",
    "cfAssistito",
    "tipoOperazione"
})
@XmlRootElement(name = "SospendiErogatoRichiesta")
public class SospendiErogatoRichiesta {

    @XmlElement(required = true)
    protected String pinCode;
    @XmlElement(required = true)
    protected String codiceRegioneErogatore;
    @XmlElement(required = true)
    protected String codiceAslErogatore;
    @XmlElement(required = true)
    protected String codiceSsaErogatore;
    @XmlElement(required = true)
    protected String pwd;
    @XmlElement(required = true)
    protected String nre;

    protected String cfAssistito;
    @XmlElement(required = true)
    protected String tipoOperazione;

    /**
     * Gets the value of the pinCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPinCode() {
        return pinCode;
    }

    /**
     * Sets the value of the pinCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPinCode(String value) {
        this.pinCode = value;
    }

    /**
     * Gets the value of the codiceRegioneErogatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceRegioneErogatore() {
        return codiceRegioneErogatore;
    }

    /**
     * Sets the value of the codiceRegioneErogatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceRegioneErogatore(String value) {
        this.codiceRegioneErogatore = value;
    }

    /**
     * Gets the value of the codiceAslErogatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAslErogatore() {
        return codiceAslErogatore;
    }

    /**
     * Sets the value of the codiceAslErogatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAslErogatore(String value) {
        this.codiceAslErogatore = value;
    }

    /**
     * Gets the value of the codiceSsaErogatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceSsaErogatore() {
        return codiceSsaErogatore;
    }

    /**
     * Sets the value of the codiceSsaErogatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceSsaErogatore(String value) {
        this.codiceSsaErogatore = value;
    }

    /**
     * Gets the value of the pwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * Sets the value of the pwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPwd(String value) {
        this.pwd = value;
    }

    /**
     * Gets the value of the nre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNre() {
        return nre;
    }

    /**
     * Sets the value of the nre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNre(String value) {
        this.nre = value;
    }

    /**
     * Gets the value of the cfAssistito property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCfAssistito() {
        return cfAssistito;
    }

    /**
     * Sets the value of the cfAssistito property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCfAssistito(String value) {
        this.cfAssistito = value;
    }

    /**
     * Gets the value of the tipoOperazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Sets the value of the tipoOperazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperazione(String value) {
        this.tipoOperazione = value;
    }

}
