
package it.finanze.sanita.dem.xsd.invioerogatoricevuta;

import it.finanze.sanita.dem.xsd.tipodati.ElencoComunicazioniType;
import it.finanze.sanita.dem.xsd.tipodati.ElencoDettagliTicketType;
import it.finanze.sanita.dem.xsd.tipodati.ElencoErroriRicetteType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nre" type="{http://tipodati.xsd.dem.sanita.finanze.it}nreType" minOccurs="0"/>
 *         &lt;element name="dataRicezione" type="{http://tipodati.xsd.dem.sanita.finanze.it}dataOraType" minOccurs="0"/>
 *         &lt;element name="codAutenticazione" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="codEsitoInserimento" type="{http://tipodati.xsd.dem.sanita.finanze.it}codEsitoType"/>
 *         &lt;element name="ElencoErroriRicette" type="{http://tipodati.xsd.dem.sanita.finanze.it}elencoErroriRicetteType" minOccurs="0"/>
 *         &lt;element name="ElencoComunicazioni" type="{http://tipodati.xsd.dem.sanita.finanze.it}elencoComunicazioniType" minOccurs="0"/>
 *         &lt;element name="calcoloEffettuato" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="ticketTotale" type="{http://tipodati.xsd.dem.sanita.finanze.it}stringType" minOccurs="0"/>
 *         &lt;element name="ElencoDettagliTicket" type="{http://tipodati.xsd.dem.sanita.finanze.it}elencoDettagliTicketType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nre",
    "dataRicezione",
    "codAutenticazione",
    "codEsitoInserimento",
    "elencoErroriRicette",
    "elencoComunicazioni",
    "calcoloEffettuato",
    "ticketTotale",
    "elencoDettagliTicket"
})
@XmlRootElement(name = "InvioErogatoRicevuta")
public class InvioErogatoRicevuta {


    protected String nre;

    protected String dataRicezione;

    protected String codAutenticazione;
    @XmlElement(required = true)
    protected String codEsitoInserimento;
    @XmlElement(name = "ElencoErroriRicette")
    protected ElencoErroriRicetteType elencoErroriRicette;
    @XmlElement(name = "ElencoComunicazioni")
    protected ElencoComunicazioniType elencoComunicazioni;

    protected String calcoloEffettuato;

    protected String ticketTotale;
    @XmlElement(name = "ElencoDettagliTicket")
    protected ElencoDettagliTicketType elencoDettagliTicket;

    /**
     * Gets the value of the nre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNre() {
        return nre;
    }

    /**
     * Sets the value of the nre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNre(String value) {
        this.nre = value;
    }

    /**
     * Gets the value of the dataRicezione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataRicezione() {
        return dataRicezione;
    }

    /**
     * Sets the value of the dataRicezione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataRicezione(String value) {
        this.dataRicezione = value;
    }

    /**
     * Gets the value of the codAutenticazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAutenticazione() {
        return codAutenticazione;
    }

    /**
     * Sets the value of the codAutenticazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAutenticazione(String value) {
        this.codAutenticazione = value;
    }

    /**
     * Gets the value of the codEsitoInserimento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEsitoInserimento() {
        return codEsitoInserimento;
    }

    /**
     * Sets the value of the codEsitoInserimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEsitoInserimento(String value) {
        this.codEsitoInserimento = value;
    }

    /**
     * Gets the value of the elencoErroriRicette property.
     * 
     * @return
     *     possible object is
     *     {@link ElencoErroriRicetteType }
     *     
     */
    public ElencoErroriRicetteType getElencoErroriRicette() {
        return elencoErroriRicette;
    }

    /**
     * Sets the value of the elencoErroriRicette property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElencoErroriRicetteType }
     *     
     */
    public void setElencoErroriRicette(ElencoErroriRicetteType value) {
        this.elencoErroriRicette = value;
    }

    /**
     * Gets the value of the elencoComunicazioni property.
     * 
     * @return
     *     possible object is
     *     {@link ElencoComunicazioniType }
     *     
     */
    public ElencoComunicazioniType getElencoComunicazioni() {
        return elencoComunicazioni;
    }

    /**
     * Sets the value of the elencoComunicazioni property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElencoComunicazioniType }
     *     
     */
    public void setElencoComunicazioni(ElencoComunicazioniType value) {
        this.elencoComunicazioni = value;
    }

    /**
     * Gets the value of the calcoloEffettuato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalcoloEffettuato() {
        return calcoloEffettuato;
    }

    /**
     * Sets the value of the calcoloEffettuato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalcoloEffettuato(String value) {
        this.calcoloEffettuato = value;
    }

    /**
     * Gets the value of the ticketTotale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketTotale() {
        return ticketTotale;
    }

    /**
     * Sets the value of the ticketTotale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketTotale(String value) {
        this.ticketTotale = value;
    }

    /**
     * Gets the value of the elencoDettagliTicket property.
     * 
     * @return
     *     possible object is
     *     {@link ElencoDettagliTicketType }
     *     
     */
    public ElencoDettagliTicketType getElencoDettagliTicket() {
        return elencoDettagliTicket;
    }

    /**
     * Sets the value of the elencoDettagliTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElencoDettagliTicketType }
     *     
     */
    public void setElencoDettagliTicket(ElencoDettagliTicketType value) {
        this.elencoDettagliTicket = value;
    }

}
