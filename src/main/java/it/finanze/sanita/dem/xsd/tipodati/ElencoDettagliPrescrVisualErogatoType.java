
package it.finanze.sanita.dem.xsd.tipodati;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for elencoDettagliPrescrVisualErogatoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="elencoDettagliPrescrVisualErogatoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioPrescrizioneVisualErogato" type="{http://tipodati.xsd.dem.sanita.finanze.it}dettaglioPrescrizioneVisualErogatoType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "elencoDettagliPrescrVisualErogatoType", propOrder = {
    "dettaglioPrescrizioneVisualErogato"
})
public class ElencoDettagliPrescrVisualErogatoType {

    @XmlElement(name = "DettaglioPrescrizioneVisualErogato", required = true)
    protected List<DettaglioPrescrizioneVisualErogatoType> dettaglioPrescrizioneVisualErogato;

    /**
     * Gets the value of the dettaglioPrescrizioneVisualErogato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dettaglioPrescrizioneVisualErogato property.
     * 
     * <p>
     * For exampleto add a new itemdo as follows:
     * <pre>
     *    getDettaglioPrescrizioneVisualErogato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DettaglioPrescrizioneVisualErogatoType }
     * 
     * 
     */
    public List<DettaglioPrescrizioneVisualErogatoType> getDettaglioPrescrizioneVisualErogato() {
        if (dettaglioPrescrizioneVisualErogato == null) {
            dettaglioPrescrizioneVisualErogato = new ArrayList<DettaglioPrescrizioneVisualErogatoType>();
        }
        return this.dettaglioPrescrizioneVisualErogato;
    }

}
