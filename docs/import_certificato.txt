# Solitamente la password del keystore è 'changeit'

# Listare i certificati presenti nel keystore
keytool -list -v -keystore keyStore.jks


##################
###### TEST ######
##################

# Eliminare il vecchio certificato (se esiste)
keytool -delete -alias demservicetest.sanita.finanze.it -keystore keyStore.jks

# Importare il nuovo certificato
keytool -import -trustcacerts -file demservicetest.pem -alias demservicetest.sanita.finanze.it -keystore keyStore.jks



##################
### PRODUZIONE ###
##################

# Keystore: D:\JBossCerts\jssecacerts
# Password keystore: Fx55BVMZJ3UMGhJ2CaEF95uU
# Path keytool: "C:\Program Files\Java\jdk1.7.0_67\bin\keytool.exe"

# Eliminare il vecchio certificato (se esiste)
keytool -delete -alias demservice.sanita.finanze.it -keystore keyStore.jks

# Importare il nuovo certificato
keytool -import -trustcacerts -file demservice_sanita_finanze_it.pem -alias demservice.sanita.finanze.it -keystore keyStore.jks
